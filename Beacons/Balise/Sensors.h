#ifndef SENSORS_H_
#define SENSORS_H_

#include "Arduino.h"
#include "Def.h"

#include <OneWire.h> 
#include <DallasTemperature.h>

int start_Sensors();
float get_Temp();
float get_Humidity();

#endif /* SENSORS_H_ */
