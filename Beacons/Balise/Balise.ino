#include "Arduino.h"
#include "Protocol.h"
#include "Def.h"
#include "Sensors.h"
#include "SDLib.h"
#include <stdlib.h>
#include <TimedAction.h> // for multi-threading

//  GLOBAL VARIABLES

ASP_GET  * get_frame;
ASP_DATA * data_frame;
ASP_GPS  * gps_frame;
GPS_Date * date_frame;
GPS_Time * time_frame;

HardwareSerial * ASP_port;
int ASP_Baud = 19200;

float tempval;
float humidval;

int error;
int new_Frame = 0;

//---------------------------------------------------------------------------------------------------------------
//                              REFRESH SENSORS
/*Threading part
TimedAction numberThread = TimedAction(700,incrementNumber);  700ms
software reset
void(* resetFunc) (void) = 0;
resetFunc();
*/

void acquire() {
  if(Serial2.available() > 0)
  {
    refresh_GPS(gps_frame, date_frame, time_frame);
  }
  tempval = get_Temp();
  humidval = get_Humidity();
}

//               ALARM TO RURN THE LED ON/OFF DURING RECEPTION
void alarm() {
  if(new_Frame == 1)
  {
      digitalWrite(ALERT_LED, HIGH);
      new_Frame = 0;
  }
  else
      digitalWrite(ALERT_LED, LOW);
}

//                          SAVE DATA INSIDE THE SD CARD
void save() {
  //ID|DATE|TIME|LAT|LON|ALT|TEMP|HUMIDITY
  String data = String();
  data = data + BALISE_ID + "|";
  data = data + date_frame->day  + "/" + date_frame->month  + "/" + date_frame->year + "|";
  data = data + time_frame->hour + ":" + time_frame->minute + ":" + time_frame->second + "|";
  data = data + gps_frame->gps_lat + "|";
  data = data + gps_frame->gps_long + "|";
  data = data + gps_frame->gps_alt + "|";
  data = data + tempval + "|";
  data = data + humidval;
  save_Data(data);
}

//                            DISPLAY DATA ON SERIAL MONITOR
void disp() {
  Serial.println("########################### ASP FRAME ##########################");
  display_GPS(gps_frame);
  display_TEMP(tempval);
  display_HUMIDITY(humidval);
  display_DATE(date_frame);
  display_TIME(time_frame);
  Serial.println("################################################################");
  Serial.println("");
}

//---------------------------------------------------------------------------------------------------------------
//                                ASSOCIATING THREADS TO FUNCTIONS

TimedAction acquireData = TimedAction(2000, acquire);
//TimedAction receiveData = TimedAction(10, receive);
TimedAction saveData = TimedAction(60000, save);
TimedAction newData = TimedAction(2000, alarm);
#ifdef VERBOSE_MODE
    TimedAction displayData = TimedAction(2000, disp);
#endif
//---------------------------------------------------------------------------------------------------------------
//                                RECEPTION PART - INTERRUPT
void receive_DATA() {
  read_Frame(get_frame, &Serial3);
  error = analyze_Frame(get_frame, gps_frame);
  Serial.print("error="); Serial.println(error);
  if(error == SUCCESS)
  {
      digitalWrite(ALERT_LED, HIGH);
      new_Frame = 1;
  }
      
  free_GET(get_frame);
}

void serialEvent3(){
  receive_DATA();
}

//---------------------------------------------------------------------------------------------------------------
void setup() {
  #ifdef VERBOSE_MODE
    Serial.begin(9600);
  #endif
  serial_start(&Serial3, ASP_Baud);
  Serial2.begin(9600);//GPS
  pinMode(3, OUTPUT); //To power the temp sensor
  pinMode(ALERT_LED, OUTPUT);
  start_Sensors();
  start_SD();
  free_SD();
  
  get_frame = malloc(GET_SIZE);
  data_frame = malloc(DATA_SIZE);
  gps_frame = malloc(GPS_SIZE);
  date_frame= malloc(4);
  time_frame=malloc(3);
}
//---------------------------------------------------------------------------------------------------------------
//                                STARTS THE THREADS
void loop() {
  //digitalWrite(ALERT_LED, LOW);
  digitalWrite(3, HIGH);
  acquireData.check();
  //receiveData.check();
  saveData.check();
  newData.check();
  #ifdef VERBOSE_MODE
      displayData.check();
  #endif
  
  delay(10);
}

