#ifndef PROTOCOL_H_
#define PROTOCOL_H_

#include "Arduino.h"
#include "Types.h"

int serial_start(HardwareSerial *port, int baud);
int read_Frame(ASP_GET * frame, HardwareSerial *port);
int analyze_Frame(ASP_GET * frame, ASP_GPS * gps);
int refresh_GPS(ASP_GPS * frame, GPS_Date * gpd, GPS_Time * gpt);
int send_GPS(ASP_GPS * frame);
int send_ACK();
int send_DATA(int command, float data);
int free_GET(ASP_GET * frame);
void display_GET(ASP_GET * frame);
void display_GPS(ASP_GPS * frame);
void display_TEMP(float temp);
void display_HUMIDITY(float humid);
void display_DATE(GPS_Date * d);
void display_TIME(GPS_Time * t);
void sleep_Balise();

#endif /* PROTOCOL_H_ */
