#ifndef DEF_H_
#define DEF_H_

//#include "Arduino.h"

/*
###########################################################################
#                          ATTENTION                                      #
#This is the configuration file, all the parameters could be changed from #
#here.                                                                    #
#                                                                         #
#*Don't forget to change BALISE_ID to the specific ID of the tag          #
#To enable/disable a parameter, just comment/uncomment it.                #
#If the parameter is a value, just change that                            #
###########################################################################
*/

#define BALISE_ID 123   //This is the Tag ID, each tag has its own ID

//configurations
//#define VERBOSE_MODE 1   //Default, enabled

//GPS Manual coordinates, uncomment those to enter manual coordinates
//GPS Coordinates: http://www.latlong.net/

//#define MANUAL_GPS 1
//#define GPS_LAT   48.418342
//#define GPS_LONG  -4.473337
//#define GPS_ALT   10.5


//Values
#define ONE_WIRE_BUS 2   //The pin corresponding to dallas temperature sensor
#define ALERT_LED    5   //The pin corresponding to the alert led
#define HUMIDITY_PIN A0  //The pin corresponding to humidity sensor
#define ACQUIRE_TIME 1  //Number of minutes, the interval of acquiring data
#define SLEEP_SECONDS 30 //The time for ASP_SLEEP command, between 0 and 30 seconds
#define TIME_ZONE 2      //Time zone with respect to GMT, France is +2
#define SD_PIN 53        //CS pin of the SD card


//Frames sizes
#define GET_SIZE    9
#define GPS_SIZE    21
#define DATA_SIZE   13


//Errors
#define SUCCESS           0
#define FAIL              -1
#define FALSE_FRAME       50
#define FALSE_DIRECTION   51
#define FALSE_ID          52
#define FALSE_CRC         53


//Getters
#define ASP_ID          100
#define ASP_TIME        101
#define ASP_GPS_DATA    102
#define ASP_TEMP        103
#define ASP_HUMIDITY    104
#define ASP_BATTERY     105   //NOT IMPLEMENTED
#define ASP_DUMP        150
#define ASP_ACK         33
#define ASP_SLEEP       99

//Setters
#define ASP_SET_TIME  201   //NOT IMPLEMENTED
//#define ASP_SET_LED   100   //NOT IMPLEMENTED

#endif /* DEF_H_ */
