#ifndef TYPES_H_
#define TYPES_H_

#include "Arduino.h"

//---------------------------------------------------------------------------------------------------------------
///The structure of ASP protocol frames
typedef struct {
  byte           pack_preamble_1 ;   //to store the preamble '$'
  byte           pack_preamble_2 ;   //to store the preamble 'A'
  byte           pack_direction;     //direction of packet
  int            balise_id;          //id of the balise
  int            pack_command;       //command of packet
  unsigned short pack_crc;           //crc
} ASP_GET;

typedef struct {
  byte           pack_preamble_1 ;   //to store the preamble '$'
  byte           pack_preamble_2 ;   //to store the preamble 'A'
  byte           pack_direction;     //direction of packet
  int            balise_id;          //id of the balise
  int            pack_command;       //command of packet
  float          pack_data;               //data
  unsigned short pack_crc;           //crc
} ASP_DATA;

typedef struct {
  byte           pack_preamble_1 ;   //to store the preamble '$'
  byte           pack_preamble_2 ;   //to store the preamble 'A'
  byte           pack_direction;     //direction of packet
  int            balise_id;          //id of the balise
  int            pack_command;       //command of packet
  float          gps_lat;            //latitude
  float          gps_long;           //longitude
  float          gps_alt;            //altitude
  unsigned short pack_crc;           //crc
} ASP_GPS;


//---------------------------------------------------------------------------------------------------------------
/*        Conversion Functions      */
/*typedef union {
  ASP_Frame * frame;
  byte stream[16];
} ASP_Serialize;*/
//---------------------------------------------------------------------------------------------------------------

typedef struct {
  uint16_t year;
  uint8_t month;
  uint8_t day;  
} GPS_Date;

typedef struct {
  uint8_t hour;
  uint8_t minute;
  uint8_t second;
  //uint8_t centisecond;  
} GPS_Time;

//---------------------------------------------------------------------------------------------------------------

/*typedef struct {
  uint16_t  temp;  
} ASP_Temp;*/


/*enum state {
  HEADER_START,
  HEADER_SECOND,
  HEADER_ARROW,
  HEADER_IDLE
};*/

#endif /* TYPES_H_ */
