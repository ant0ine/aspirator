# -*- coding: utf-8 -*-
"""
Created on Sat Apr 29 13:52:48 2017

@author: Jean
"""

import sys
import os
sys.path.append('./src')
sys.path.append("./RaspberryPi/src")
sys.path.append('./../../RaspberryPi/src')
from mainguidage import *
from asp import *
from job import *
import threading
import time


def choix(frame):
    global ListeGPSavion, ListeBalise, IDbeacon, t, testdate, EtatBalise, liste_id
    # Numéro des commandes
    # ID = 100
    # TIME = 101
    # GPS = 102
    # TEMP = 103     # Temperature
    # BATTERY = 104 
    # Données collectées = 99

    command = frame[4]
    msg = frame[5]
    id_beacon = frame[3]
    if len(liste_id) == 0 or liste_id[-1] != id_beacon:
        print("\n########################################\n \t   Balise id : %i \n########################################"
                %(id_beacon))

    liste_id.append(id_beacon)
    # ListeGPSavion = []
    # ListeBalise = []
    # IDbeacon = []
    
    if command == 100 :
        print("L'ID de la balise est ",msg)
        catégorie = 'ID'
        
    elif command == 101 : 
        print("Heure : ",msg)
        catégorie = 'TIME'
        
    elif command == 102 :
        if id_beacon == 0: #Coordonnées GPS de l'avion
            ListeGPSavion.append((frame[5],frame[6]))
            main2(ListeGPSavion,EtatBalise,ListeBalise)
            plt.pause(t)
            catégorie = 'GPS Avion'
            
        else :
            catégorie = 'GPS Balise'
            if id_beacon in IDbeacon:
                pass
            else :
                IDbeacon.append(id_beacon)
                ListeBalise.append(Balise(id_beacon,frame[5],frame[6]))
                EtatBalise.append("detectee")
                      
    elif command == 103 :
        print("La température est de ",msg," degré")
        catégorie = 'TEMP'
        
    elif command == 101 : 
        print("Heure : ",msg)
        catégorie = 'TIME'
        
    elif command == 102 :
        if id_beacon == 0: #Coordonnées GPS de l'avion
            ListeGPSavion.append((frame[5],frame[6]))
            main2(ListeGPSavion,EtatBalise,ListeBalise)
            plt.pause(t)
            catégorie = 'GPS Avion'
            
        else :
            catégorie = 'GPS Balise'
            if id_beacon in IDbeacon:
                pass
            else :
                if frame[5]==0 and frame[6]==0:
                    print('données GPS de la balise égale à 0, le GPS ne fonctionne pas')
                else :
                    IDbeacon.append(id_beacon)
                    ListeBalise.append(Balise(id_beacon,frame[5],frame[6]))
                    EtatBalise.append("detectee")
            
    elif command == 103 :
        print("La température est de ",msg," degré")
        catégorie = 'TEMP'
        
    elif command == 104 : 
        print("HUMIDITY de la balise : ", msg)
        catégorie = 'HUMIDITY'
        
    elif command == 909 :
        print("Batterie de la balise : ", msg)
        catégorie = 'BATTERY'
        
    elif command == 99 :
        k = 0
        
        for i in IDbeacon :
            if i == id_beacon:
                m = k
                
            k += 1
        print("Données récupérées ! \n")
        str_id = str(id_beacon)
        message_espeak = 'espeak "données récupérées de la balise "' + str_id + '"" -v french -s 130 -p 50'
        print(message_espeak)
        os.system(message_espeak)
        EtatBalise[m] = "données récupérées"
        catégorie = 'Validation données'
    else:
        catégorie = str(command)
    
    chemin1 = './DATA/donneesBalise' + str(id_beacon) +'.txt' 
    chemin2 = './DATA/donneesGPSavion.txt'
    fichierDonnées = open(chemin1,'a')
    fichierAvion = open(chemin2,'a')
    

    if id_beacon in testdate : # Ecrit la date à chaque fois qu'on redémarre le programme
        pass
    else :
        fichierDonnées.write('\n'+str(time.asctime( time.localtime(time.time()) ))+'\n')
        testdate.append(id_beacon)
    
    
    if command == 102 and id_beacon == 0 :
        a = catégorie + ' : longitude (x) : ' + str(frame[5]) + ' latitude (y) : '+ str(frame[6]) +'\n'
        fichierAvion.write(a)
    elif command == 102 and id_beacon != 0:
        a = catégorie + ' : longitude (x) : ' + str(frame[5]) + ' latitude (y) : '+ str(frame[6]) +'\n'
        fichierDonnées.write(a)
    else :
        a = catégorie + ' : ' + str(msg) +'\n'
        fichierDonnées.write(a)
    fichierDonnées.close()
    fichierAvion.close()
 
def f():
    num = input("n° du port APC avion : ")
    if os.name == "nt":
        portname = "COM" + str(int(num))
    else:
        portname = "/dev/ttyUSB" + str(int(num))
 
    apc = ASP(portname, baudrate=9600, identity='beacon', name="Listener")
    print("ouverture OK ")
    while True :
        # Choix(apc.Rx())  
        choix(reception(apc))
        time.sleep(0.1)

def reception(apc):
    coded_frame = apc.serial.read(ASP.GPS_SIZE)
    # print(coded_frame)
    decoded_frame = struct.unpack(ASP.GPS_FRAME, coded_frame)
    print(decoded_frame)
    # print("reception GET")
    # print(decoded_frame)
    sleep(0.1)

    id_beacon = decoded_frame[3]
    command = decoded_frame[4]

    # if command == ASP.GPS:
    #     coded_frame = apc.serial.read(ASP.GPS_SIZE)
    #     print("reception GPS")
    #     decoded_frame = struct.unpack(ASP.GPS_FRAME, coded_frame)
    # else:
    #     coded_frame = apc.serial.read(ASP.DATA_SIZE)
    #     print("reception DATA")
    #     decoded_frame = struct.unpack(ASP.DATA_FRAME, coded_frame)
    # print(decoded_frame)
    # sleep(0.1)
    
    return decoded_frame

        
def Erasefile():
    global IDbeacon
    for i in IDbeacon :
            chemin1 = './DATA/donneesBalise' + str(i) +'.txt' 
            fichierDonnées = open(chemin1,'w')
            fichierDonnées.close()
    chemin1 = './DATA/donneesBalise' + str(0) +'.txt' 
    fichierDonnées = open(chemin1,'w')
    fichierDonnées.close()    
    chemin2 = './DATA/donneesGPSavion.txt'            
    fichierAvion = open(chemin2,'w')
    fichierAvion.close()

"""
Idée :
    Au démarrage du programme la premiere fois, on a pas de balise donc risque que le programme de guidage plante,
    ou en tout cas il ne peut pas tourner ! => lancer le guidage uniquement quand on a une balise. Une fois que les données 
    de cette balise sont acquises et qu'il faut aller sur une autre balise, supprimer le guidage jusqu'a recapter
    les coordonnées d'une autre.
    
    Pour les autres démarrages, lire dans les fichiers texte les anciennes coordonnées des balises
"""
  
def test():
    yb1, xb1 = 48.418511, -4.473964 #Coord milieu Stade
    yb2, xb2 = 48.418146, -4.473550
    yb3 = 48.4189
    xb3 = 4.4734  
#    a = [-4.474470,48.417789]
    a =[-4.472336,48.41798]
    b = [4.4733,48.4183]
    c = [4.4736,48.41827]
    d = [4.4742,48.41816]
    e = [4.4744,48.41808]       #Balise 1
    
    t = 0.5 #temps entre chaque guidage
    
    
    j=0.00005
    p=0.0001
    
    
   
    ###########################################
    #                                         #
    #        Paramètrage liste balises        #
    #                                         #
    ###########################################
    
    ListeBalise = [Balise(1,xb1,yb1)]
#    Balise(2,xb2,yb2),Balise(1,xb1,yb1)
    
    ListeGPSavion = []
    IDbeacon = []
    if len(ListeBalise) != 0 and IDbeacon == []:
        for i in range(len(ListeBalise)):
            IDbeacon.append(ListeBalise[i].id)

    testdate = []
    nbreBalise = len(ListeBalise)
    EtatBalise = nbreBalise*["non_detectee"]
    
#    frame = [0,1,2,1,102,xb1,yb1]
#    choix(frame)
#    m
#    frame = [0,1,2,2,102,xb2,yb2]
#    choix(frame)    

    frame = [0,1,2,0,102,a[0],a[1]]
    choix(frame)
    
    a[0]+= j
    a[1]+=p
    frame = [0,1,2,0,102,a[0],a[1]]
    choix(frame)
    
    
    a[0]+= j
    frame = [0,1,2,0,102,a[0],a[1]]
    choix(frame)
    
    a[0]+= j
    frame = [0,1,2,0,102,a[0],a[1]]
    choix(frame)
    
    frame = [0,1,2,1,99,0]
    choix(frame)
    
    a[0]+= j
    a[1]-=p
    frame = [0,1,2,0,102,a[0],a[1]]
    choix(frame)
      
    a[0]+= j
    a[1]-=p
    frame = [0,1,2,0,102,a[0],a[1]]
    choix(frame)
       
    a[0]+= j
    a[1]+=p
    frame = [0,1,2,0,102,a[0],a[1]]
    choix(frame)
    
    a[0]+= j
    a[1]-=p
    frame = [0,1,2,0,102,a[0],a[1]]
    choix(frame)
    
    a[0]-= j
    a[1]-=p
    frame = [0,1,2,0,102,a[0],a[1]]
    choix(frame)
  
    a[0]-= j
    a[1]-=p
    frame = [0,1,2,0,102,a[0],a[1]]
    choix(frame)
    
    a[0]-= j
    frame = [0,1,2,0,102,a[0],a[1]]
    choix(frame)


    

    
if __name__ == "__main__":
    t = 0.01 #temps entre chaque guidage
#    yb1, xb1 = 48.418511, -4.473964
#    yb2, xb2 = 48.418146, -4.473550

    xb1,yb1 = -4.472436428070068, 48.418418884277344
    xb2, yb2 = -4.473337173461914, 48.41834259033203
#    yb3 = 48.4189
#    xb3 = 4.4734  
#    a = [48.41824,4.47285]
#    b = [48.418212,4.472996]
#    c = [48.41818,4.473]
#    d = [48.41816,4.4732]
#    e = [48.41808,4.4744] 
    
    ###########################################
    #                                         #
    #        Paramètrage liste balises        #
    #                                         #
    ###########################################
    
    ListeBalise = []
#    Balise(1,xb1,yb1),Balise(2,xb2,yb2)
    
  
    # Balise(2,xb2,yb2),Balise(1,xb1,yb1)
    testdate = []
    ListeGPSavion = []
    IDbeacon = []
    liste_id = []
    if len(ListeBalise) != 0 and IDbeacon == []:
        for i in range(len(ListeBalise)):
            IDbeacon.append(ListeBalise[i].id)

    nbreBalise = len(ListeBalise)
    EtatBalise = nbreBalise*["non_detectee"]
    
   
    
    main_parse = threading.Thread(name="main", target=f)
    main_parse.start()

    # test()
    # Erasefile()
    
    plt.show()
