#!/usr/bin/python3
# -*- coding: Utf-8 -*-

import time
import serial
import numpy as np 
import matplotlib.pyplot as plt



class Guidage():
    def __init__(self,lon,lat):
        self.ser = None
        self.x = lon #pourquoi ??? x et y position de l'avion récupérées depuis le gps
        self.y = lat #pourquoi ???
        
def affichage(s, avion, liste_coin):
    xmin = liste_coin[0]
    xmax = liste_coin[1]
    ymin = liste_coin[2]
    ymax = liste_coin[3]

    s = np.array(s)
    x = s[:,0]
    y = s[:,1]
    plt.xlim(xmin, xmax)
    plt.ylim(ymin, ymax)
    plt.plot(x,y,'bo')
    
    for b in avion.list_b:
#        print(b.etat)
        if b.etat == "en_cours":
            plt.plot(b.x, b.y, 'or')
        elif b.etat == "données récupérées":
            plt.plot(b.x, b.y, 'og')
        else:
#            print("hello")
            plt.plot(b.x, b.y, 'om')
    plt.pause(0.0001)
    
def trajet(liste_position, dt, guidage, avion, liste_coin,fichierText,n, ListeBalise):
    liste_position.append([avion.x1, avion.y1]) #pos avion
    liste_position.append([avion.x2, avion.y2])

#    while avion.i < len(avion.list_b):
#    avion.x1 = avion.x2
#    avion.y1 = avion.y2
    #guidage.connectGPS("COM4")
#    guidage.readXbee(fichierText)
#    avion.x2 = guidage.x       # Guidage, variable à corriger
#    avion.y2 = guidage.y

    lat = (fichierText[n][1])
    lon = (fichierText[n][0])
    avion.x2 = lon      # Guidage, variable à corriger
    avion.y2 = lat 
    avion.correction(ListeBalise)
    print('x=%f, y=%f' %(avion.x2, avion.y2))
    
    liste_position.append([avion.x2, avion.y2])
    affichage(liste_position, avion,liste_coin)
    time.sleep(0.001)