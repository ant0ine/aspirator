#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 14 11:08:37 2017

@author: aspirator
"""

class Balise():
    def __init__(self, i, x, y,e="non_detectee", d=None):
        self.id = i
        self.x = x
        self.y = y
        self.etat = e #detectee, en_cours, trouvee
        self.donnee = d
