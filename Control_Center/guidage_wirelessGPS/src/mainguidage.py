#!/usr/bin/python3
# -*- coding: Utf-8 -*-

import sys
import serial
import time
import numpy as np 
import matplotlib.pyplot as plt
sys.path.append("./src/")
sys.path.append("./enregistrements/")

from balise import Balise
from avion import Avion
from guidage import Guidage, trajet
import os
from numpy import *
from matplotlib.pyplot import *
#from menage import *

def main1(ListeGPSavion, EtatBalise, ListeBalise):

        n = len(ListeGPSavion) - 1
        guid = Guidage(ListeGPSavion[n-1][0],ListeGPSavion[n-1][1])
                
#Stade
       # ymin, xmin = 48.417789, -4.474470 
       # ymax, xmax = 48.419266, -4.473450 
    
#Place d'arme
        ymin, xmin = 48.418301, -4.472656
         
        ymax, xmax = 48.418709, -4.472161

#Ensta
       # ymin, xmin = 48.417789, -4.474470
       # ymax, xmax = 48.419872, -4.471961 


        x1 = guid.x
        y1 = guid.y

        if len(ListeBalise) == 0:
            axis([xmin,xmax,ymin,ymax])
            plot(ListeGPSavion[n][0],ListeGPSavion[n][1],'bo')
            return EtatBalise
        else :
            av = Avion(ListeBalise,x1,y1,ListeGPSavion[n][0],ListeGPSavion[n][1])
        
        i=0
        for b in av.list_b :
            b.etat = EtatBalise[i] 
            i+=1
            if b.etat == 'en_cours': #On vérifie si un guidage est en cours ou pas
                av.b = b
            

        #Algo détection le plus proche
        d = 10**3        
        if av.b.etat == 'données récupérées':
            for b in av.list_b:
                db= np.sqrt((b.x-av.x2)**2 + (b.y-av.y2)**2)
                if (db <= d and b.etat == "detectee"):
                    d = db
                    av.b = b
                    

        trajet([], 0.1, guid, av,[xmin, xmax, ymin, ymax],ListeGPSavion,n, ListeBalise)
        i=0
        for b in av.list_b :
            EtatBalise[i] = b.etat
            i+=1

        return EtatBalise
        

def main2(ListeGPSavion,EtatBalise,ListeBalise):
        
    k = 1
    if ListeBalise == []:
        print("Aucunes balises n'a été détectées")
    if len(ListeGPSavion)>1 :
            for i in EtatBalise :                
                if i != 'données récupérées':
                    k = 0
            if k==0:
                EtatBalise = main1(ListeGPSavion,EtatBalise,ListeBalise)
            if k == 1:
                EtatBalise = main1(ListeGPSavion,EtatBalise,ListeBalise) 
#                    print("Mission terminée")
#                    os.system('espeak "Mission terminée" -v french -s 130 -p 50')
        
    else :
        print("attente de plus de données avant de faire un guidage")
      
    """
    for i in EtatBalise :
        if i != 'informations récupérées':
            EtatBalise = main1(ListeGPSavion,EtatBalise)
        else :
            print("Toutes les données ont été récupérées, retour à la base !")
            
    """
    
