import threading
import time
import logging
import random
try:
    import Queue            # Python 2.7
except:
    import queue as Queue   # Python 3.3

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s',)

BUF_SIZE = 10
q = Queue.Queue(BUF_SIZE)

class ProducerThread(threading.Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        super(ProducerThread,self).__init__()
        self.target = target
        self.name = name

    def run(self):
        while True:
            if not q.full():
                item = random.randint(1,10)
                q.put(item)
                logging.debug('Putting ' + str(item)  
                              + ' : ' + str(q.qsize()) + ' items in queue')
                time.sleep(random.random())
        return

class ConsumerThread(threading.Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        super(ConsumerThread,self).__init__()
        self.target = target
        self.name = name
        return

    def run(self):
        while True:
            if not q.empty():
                item = q.get()
                logging.debug('Getting ' + str(item) 
                              + ' : ' + str(q.qsize()) + ' items in queue')
                #time.sleep(random.random())
        return

class ProducerConsumerThread(threading.Thread):
    def __init__(self, identity, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        super(ProducerConsumerThread,self).__init__()
        self.target = target
        self.name = name
        self.id = identity
        return

    def run(self):
        if self.id == 'producer':
            self.runProducer()
        else:
            self.runConsumer()

    def runProducer(self):
        print("start producer")
        while True:
            if not q.full():
                item = random.randint(1,10)
                q.put(item)
                logging.debug('Putting ' + str(item)  
                              + ' : ' + str(q.qsize()) + ' items in queue')
                time.sleep(random.random())
        return

    def runConsumer(self):
        print("start consumer")
        while True:
            if not q.empty():
                item = q.get()
                logging.debug('Getting ' + str(item) 
                              + ' : ' + str(q.qsize()) + ' items in queue')
                time.sleep(random.random())
        return

def runProducerAlone():
    global q
    print("start producer")
    while True:
        if not q.full():
            item = random.randint(1,10)
            q.put(item)
            logging.debug('Putting ' + str(item)  
                          + ' : ' + str(q.qsize()) + ' items in queue')
            time.sleep(random.random())
    return

def runConsumerAlone():
    global q
    print("start consumer")
    while True:
        if not q.empty():
            item = q.get()
            logging.debug('Getting ' + str(item) 
                          + ' : ' + str(q.qsize()) + ' items in queue')
            time.sleep(random.random())
    return

if __name__ == '__main__':
    
    ## Méthode 1
    p = ProducerThread(name='producer')
    c = ConsumerThread(name='consumer')

    ## Méthode 2
    p = threading.Thread(name='producer', target=runProducerAlone)
    c = threading.Thread(name='consumer', target=runConsumerAlone)

    ## Méthode 3
    p = ProducerConsumerThread(identity='producer', name='producer')
    c = ProducerConsumerThread(identity='consumer', name='consumer')
    
    ## Execution
    p.start()
    time.sleep(2)
    c.start()
    time.sleep(2)        
