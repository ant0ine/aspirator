#! /usr/bin/env python3
# coding:utf-8

"""
This file is not as clean as it should be. I'm too lazy to rewrite since the
project ended in May 2017.
The RECEPTION part is not used from this file anymore. Check fsm.py for more
details about that.
"""

import serial
import threading
import os
import struct
from time import sleep
from job import Job

try:
    import Queue            # Python 2.7
except:
    import queue as Queue   # Python 3.3

class ASP(threading.Thread):
    """
    ASP Class
    Aspirator Serial Protocol
    """
    rxbuff = bytearray()
    RxQ    = Queue.Queue()
    stop   = threading.Event()
    rx     = True

    BUF_SIZE = 100
    aspbuff  = Queue.PriorityQueue(BUF_SIZE)

    SLEEP    = 99
    ID       = 100
    TIME     = 101
    GPS      = 102
    TEMP     = 103     # Temperature
    HUMIDITY = 104

    GET_FRAME  = '<BBBHHH'
    GPS_FRAME  = '<BBBHHfffH'
    DATA_FRAME = '<BBBHHfH'
    
    GET_SIZE  = 9
    GPS_SIZE  = 21
    DATA_SIZE = 13

    ###################################################
    ###                                             ###
    ###              INITIALISATION                 ###
    ###                                             ###
    ###################################################
    def __init__(self, serialport, identity, baudrate=19200, polynome='1011', 
                 group=None, target=None, name=None, args=(), kwargs=None,
                 verbose=None, timeout=None):
        #threading.Thread.__init__(self)
        super(ASP, self).__init__(name=name, group=group, target=target, 
                                  args=args, kwargs=kwargs)

        self.serial = serial.Serial(port=serialport, baudrate=baudrate, timeout=timeout)
        self.polynomeCRC = polynome
        self.identity = identity    # 'beaconRX' / 'controlcenterTX' / 'planeRX'

    def shutdown(self):
        self.stop.set()
        self.serial.close()
        self.join()
        print("Closing the serial port !")

    def run(self):
        # print("Running !")
        if self.identity == 'beaconRX':
            self.runReceptionFromBeacon()
        elif self.identity == 'controlcenterTX':
            self.runEmissionToControlCenter()
        elif self.identity == 'planeRX':
            self.runReceptionFromPlane()

    ###################################################
    ###                                             ###
    ###                 THREADING                   ###
    ###                                             ###
    ###################################################
    def runReceptionFromBeacon(self):
        print("Starting thread : ", self.getName())
        while not self.stop.is_set():
            self.Rx()
            sleep(0.01)

    def runEmissionToControlCenter(self):
        """ 
        This function reads the priority queue, gets the 
        element to transfer them to the control center
        """
        print("Starting thread : ", self.getName())
        while True:
            if not self.aspbuff.empty():
                # print("@@@@@@@@@@@@@@@@ VIDAGE")
                frame = self.aspbuff.get()
                #print("Controle Center : ", frame.description, "\n")
                self.serial.write(frame.description)
                sleep(0.01)

    ###################################################
    ###                                             ###
    ###                 RÉCEPTION                   ###
    ###                                             ###
    ###################################################
    def receiveMessage(self, wait=5):
        """
        Checks receive queue for messages

        Inputs:
            wait(optional): Desired number of seconds to wait for a message
        Output:
            Message, if received.  None if timed out.
        """
        try:
            return self.RxQ.get(timeout=wait)
        except Queue.Empty:
            return None 

    def Rx(self, size=15):
        """
        if self.serial.inWaiting():
            remaining = self.serial.inWaiting()
            while remaining > 0:
                chunk = self.serial.read(remaining)
                print("remaining : ", remaining)
                print("chunk : ", chunk)
                remaining -= len(chunk)
                self.rxbuff.extend(chunk)
                #self.rxbuff += chunk
        """
        self.rxbuff = self.serial.read(size)
        # self.rxbuff = self.serial.readline()[:-3]
        print("rxbuff : ", self.rxbuff)
        #self.rxbuff = bytearray()

        # If the frame is valid according to our protocol
        if self.validate(self.rxbuff):
            print("test succeded !")
            
            ## Unpack
            frame = self.rxbuff
            self.rxbuff = bytearray()
            decoded_frame = struct.unpack(ASP.STD_FRAME, frame)
            preamble1 = decoded_frame[0]
            preamble2 = decoded_frame[1]
            direction = decoded_frame[2]
            id_beacon = decoded_frame[3]
            command = decoded_frame[4]
            message = decoded_frame[5]
            crc = decoded_frame[6]
            print("Message reçu : ", message)
            print('id', id_beacon)
            ## Send acknowledgement
            self.sendAck(id_beacon)

            ## Put in queue for transmission
            if not self.aspbuff.full():
                if command == ASP.GPS:
                    priority = 2
                else:
                    priority = 3
                self.aspbuff.put(Job(priority, frame))
                print("@@@@@@@@@@@@@@@@ REMPLISSAGE")
        else:
            print("test failed !")
            #self.shutdown()

    ###################################################
    ###                                             ###
    ###                 VALIDATION                  ###
    ###                                             ###
    ###################################################
    def validate(self, decoded_frame):
        """
        Check that the decoded frame is an ASP frame by checking the preamble
        """
    
        ## Check is Aspirator protocol
        list_letter = [ord('A'), ord('B')]
        list_symbol = [ord('>'), ord('<')]
        print('$', decoded_frame[0], ord('$'), 'A/B : ', decoded_frame[1], ord('B'),'symbol : ', decoded_frame[2], '<', ord('<'), '>', ord('>'))
        # if decoded_frame[0] != ord('$') \
        #     or decoded_frame[1] != ord('B') :\
        #     # or decoded_frame[2] != ord('<') :# or decoded_frame[2] != ord('>'):
        if decoded_frame[0] != ord('$') \
            or decoded_frame[1] not in list_letter \
            or decoded_frame[2] not in list_symbol:
                print("fail protocol")
                return (False)
    
        ## All checks are passed
        # print("Rx : ", decoded_frame)
        return (True) 

    def sendAck(self, id_dest, preamble2='B'):
        """
        Send an aknowledgement to the beacon after having received a good frame
        """
        #msg_bin = ""
        #for i in message:
        #    msg_bin += bin(ord(i))[2:]
        #print("OK! bin : ", msg_bin)
        """
        message = ord('!')
        preamble = ord('$')
        preamble2 = ord('A')
        direction = ord('>')
        """
        preamble = '$'
        # preamble2 = 'B'
        direction = '>'
        message = 33
        data = bin(ord(preamble))[2:] +\
                bin(ord(preamble2))[2:] +\
                bin(ord(direction))[2:] +\
                bin(id_dest)[2:] +\
                bin(message)[2:]
        #print("str : ", str(data))

        crc = self.computeCRC(data)
        #print("message : ", message)
        #print("crc : ", crc, type(crc), int(crc, 2))
        data = struct.pack(ASP.GET_FRAME, 
                            ord(preamble),
                            ord(preamble2),
                            ord(direction), 
                            id_dest, 
                            message,
                            int(crc, 2))
        print("ACK : ", preamble, preamble2, direction, id_dest, message, crc)
        print("#########################################\n")
        self.serial.write(data)

    def computeCRC(self, message, poly='1011', code_verification='', compute=True):
        """
        DEPRECATED
        Next version implements CRC verification and computation
        """
        #CRC-16 polynomial
        if compute and len(code_verification) != len(poly)-1  \
                    and code_verification != '0'*(len(poly) - 1):
                        code_verification = '0'*(len(poly) - 1)
        message = message + code_verification
        message = list(message)
        poly = list(poly)

        for i in range(len(message)-len(code_verification)):
            if message[i] == '1':
                for j in range(len(poly)): 
                    # operation xor <=> modulo 2 
                    message[i+j] = str((int(message[i+j])+int(poly[j]))%2)

        #print (" message ", message)
        return ''.join(message[-len(code_verification):])

    ###################################################
    ###                                             ###
    ###                 ÉMISSION                    ###
    ###                                             ###
    ###################################################
    def sendMessageGET(self, preamble_letter, msg, id_dest):
        frame = self.packMessageGET(preamble_letter, msg, id_dest)
        return self.serial.write(frame)

    def sendMessageGPS(self, preamble_letter, GPS, id_dest):
        frame = self.packMessageGPS(self, preamble_letter, GPS, id_dest)
        return self.serial.write(frame)

    def sendMessageData(self, preamble_letter, data, id_dest):
        frame = self.packMessageData(preamble_letter, data, id_dest)
        return self.serial.write(frame)

    def packMessageGET(self, preamble_letter, msg, id_dest):
        # the <Lhhhb  is interpreted as follows
        #   see https://docs.python.org/2/library/struct.html
        #     <   little-endian
        #     L   unsigned long
        #     h   signed short
        #     B   unsigned char - same as Arduino BYTE

        preamble = '$'
        preamble2 = preamble_letter
        direction = '>'
        command = msg

        print("Tx GET : ", preamble, preamble2, direction, id_dest, command, "crc")
        data = bin(ord(preamble))[2:] +\
                bin(ord(preamble2))[2:] +\
                bin(ord(direction))[2:] +\
                bin(id_dest)[2:] +\
                bin(command)[2:]

        crc = self.computeCRC(data)
        data = struct.pack(ASP.GET_FRAME,  
                            ord(preamble), 
                            ord(preamble2), 
                            ord(direction),
                            id_dest, 
                            command, 
                            int(crc, 2))     

        print("Tx: ", data, "\n") 
        return (data)

    def packMessageGPS(self, preamble_letter, GPS, id_dest, command=102):
        preamble = '$'
        preamble2 = preamble_letter
        direction = '>'
        command = command
        
        lon = GPS[0]
        lat = GPS[1]
        alt = GPS[2]
        
        print("Tx GPS : ", preamble, preamble2, direction, id_dest, command, lat, lon, alt, "crc")
        
        crc = '101'     # This is implement in the next version
        data = struct.pack(ASP.GPS_FRAME, 
                           ord(preamble),
                           ord(preamble2),
                           ord(direction),
                           id_dest,
                           command,
                           lon,
                           lat,
                           alt,
                           int(crc, 2))     

        print("Tx GPS : ", data, "\n") 
        return (data)

    def packMessageData(self, preamble_letter, command, data_tx, id_dest):
        # TODO : tests

        preamble = '$'
        preamble2 = preamble_letter
        direction = '>'

        print("Tx DATA : ", preamble, preamble2, direction, id_dest, command, data_tx, "crc")
        crc = '101'
        data = struct.pack(ASP.DATA_FRAME,  
                            ord(preamble), 
                            ord(preamble2), 
                            ord(direction),
                            id_dest, 
                            command,
                            data_tx, 
                            int(crc, 2))     

        print("Tx DATA_FRAME : ", data, "\n") 
        return (data)        


###################################################
###                                             ###
###                     GPS                     ###
###                                             ###
###################################################
def getGPS(serialport, apc, baudrate=9600):
    try:
        gps = serial.Serial(serialport, baudrate)
        # For debugging purposes in the train
        #gps = 1
    except:
        print("error while opening GPS serial connection : ")
    else:
        print("ouverture ok")
        q = ASP.aspbuff

        while True:
            datastr = ''
            # For debugging purposes in the train
            gps_frame = gps.readline()
            #gps_frame = 1
            try: 
                datastr += gps_frame.decode('utf-8')
                #datastr = ''
            except UnicodeDecodeError:
                continue
            else:
                #print(datastr)
                ## For debugging purpose in the train :
                #datastr = '$GPGGA,160214.00,4825.07283,N,00428.38364,W,1,03,4.05,62.3,M,50.7,M,,*78'

                if datastr[:6] == '$GPGGA':
                    message = traitementGPGGA(datastr)
                    priority = 1
                    print("GGA : ", message)

                    ## Tell the control center that a GPS message is coming
                    frame = apc.packMessageGPS(preamble_letter='A', GPS=message, id_dest=0)
                    q.put(Job(priority, frame))
                    print("@@@@@@@@@ REMPLISSAGE")
                    sleep(0.1)

def traitementGPGGA(ligne):
    """
    TODO : Can't remember if data should be send as lat, lon, alt ou lon, lat, alt
            I know, that in the end, with the GUI it has been fixed, but I don't 
            remember where :/
    TODO : Test on the proposed fix
    """
    ligne = ligne.split(",")
    for k in range(len(ligne)):
        if ligne[k] == '':            
            ligne[k] = ligne[k].replace('','0') # les données absentes des trames
                                                # sont remplacées par des données nulles
    # print(ligne)
    try:
        lat = ligne[2]
        #print("lat ", lat)
        lat = float(lat[0:2])+(float(lat[2:])/60) # les latitudes et longitudes sont
        if ligne[3] == 'S':
            lat = -lat
    except ValueError:
        #pass
        lat = 0
    # exprimées uniquement en degrés pour être utilisable par transform.
    
    try:
        lon = ligne[4]
        lon = float(lon[0:3])+(float(lon[3:])/60)
        if ligne[5] == 'W':
            lon = -lon
    except ValueError:
        #pass
        lon = 0
    altitude = float(ligne[9])

    return (lon, lat, altitude)

###################################################
###                                             ###
###                INTERROGATION                ###
###                                             ###
###################################################
def main(apc):
    ## TESTS TODO
    id_beacon = 123

    play = True
    while play:
        try:
            apc.sendMessageGET(ASP.TEMP, id_beacon)
            #apc.receiveMessage()
        except KeyboardInterrupt:
            play = False
    apc.shutdown()

###################################################
###                                             ###
###                    MAIN                     ###
###                                             ###
###################################################
if __name__ == "__main__":
    #########################################
    ###               BEACONS             ###
    #########################################
    num = input("n° du port APC balise: ")
    if os.name == "nt":
        portname = "COM" + str(int(num))
    else:
        portname = "/dev/ttyUSB" + str(int(num))

    apc = ASP(portname, identity='beaconRX', name="Listening Beacons", baudrate=19200)
    apc.start()

    #########################################
    ###               TX to CC            ###
    #########################################
    # num = input("n° du port APC control center: ")
    # if os.name == "nt":
    #     portname = "COM" + str(int(num))
    # else:
    #     portname = "/dev/ttyUSB" + str(int(num))
    apc_CC = ASP('/dev/ttyUSB1', identity='controlcenterTX', name='Talking to CC', baudrate=19200)
    apc_CC.start()

    #########################################
    ###               QUESTIONS           ###
    #########################################
    # main = threading.Thread(name="main", target=main, args=(apc,))
    # main.start()

    #########################################
    ###               GET GPS             ###
    #########################################
    num = input("n° du port du GPS: ")
    if os.name == "nt":
        portname = "COM" + str(int(num))
    else:
        portname = "/dev/ttyUSB" + str(int(num))
    #portname = '/dev/ttyUSB0'
    get_gps = threading.Thread(name='Monitoring_GPS', target=getGPS, args=(portname, apc,))
    get_gps.start()
