#! /usr/bin/env python3
# coding:utf-8
"""
Project: ASPIRATOR
ENSTA Bretagne 2016-2017
@author:ant0ine
"""
import serial
import threading
import os
import time
import struct

from asp import *


class FSM(threading.Thread):
    """
    Class that be the 'brain' of the RaspberryPi
    It uses a state machine to know what to do
    """
    
    stop = threading.Event()

    def __init__(self, group=None, target=None, name=None, 
                args=(), kwargs=None, verbose=None):
        super(FSM, self).__init__(name=name, group=group, target=target, 
                                  args=args, kwargs=kwargs)   
        self.apc = None                 # ASP Object to deal with the beacons
        self.apc_CC = None              # ASP Object to deal with the Control Center
        self.gps = None                 # Threading Object to deal with the GPS
        self.state = 'init'             # Initial state of the FSM
        self.beacon_id = None           # Indicates the id of the beacon we are
                                        # speaking with
        self.beacon_nb_sensors = 2      # Hard coded for the moment, 
                                        # number of sensors on the beacons
        self.beacon_sensors_state = [0]*self.beacon_nb_sensors # State of the 
                                        # different sensors 
                                        # 0 : unknown 
                                        # 1 : asked 
                                        # 2 : data retrieved
        self.beacon_sensor = 0          # Index of the sensor we focus on, 
                                        # used with the list above. Each value of
                                        # the list corresponds to one sensors
        self.nb_of_reception = 0        # Number of sensors we interrogated
        self.msg = None
        self.failsafe = 0               # Failsafe : allow quiting a state and 
                                        # going back to the previous one if one
                                        # infinite loop occurs

    def run(self):
        while not self.stop.is_set():
            if self.state == 'init':
                print("~~~~~~~~ INIT")
                self.init()

            elif self.state == 'starting':
                print("~~~~~~~~ STARTING")
                self.startMission()

            elif self.state == 'searching':
                print("~~~~~~~~ SEARCHING")
                self.searching()

            elif self.state == 'waitingPing':
                print("~~~~~~~~ WAITING PING")
                self.waitingPing()

            elif self.state == 'askingData':
                print("~~~~~~~~ ASKING DATA")
                self.askingData()

            elif self.state == 'waitingData':
                print("~~~~~~~~ WAITING DATA")
                self.waitingData()

            elif self.state == 'receivingFile':
                print("~~~~~~~~ RECEIVING FILE")
                self.receivingFile()
            
            elif self.state == 'finishingCommunication':
                print("~~~~~~~~ FINISHING COMMUNNICATION")
                self.finishingCommunication()
            
            elif self.state == 'quiting':
                """
                Shuting down everything. Mission has ended.
                """
                self.shutdown()
            else:
                continue

    def init(self):
        #########################################
        ###               BEACONS             ###
        #########################################
        num = input("n° du port APC balise: ")
        if os.name == "nt":
            portname = "COM" + str(int(num))
        else:
            portname = "/dev/ttyUSB" + str(int(num))

        self.apc = ASP(portname, identity='beaconRX', name="Listening Beacons", baudrate=19200, timeout=1)

        #########################################
        ###               TX to CC            ###
        #########################################
        num = input("n° du port XBee control center: ")
        if os.name == "nt":
            portname = "COM" + str(int(num))
        else:
            portname = "/dev/ttyUSB" + str(int(num))
        self.apc_CC = ASP(portname, identity='controlcenterTX', name='Talking to CC', baudrate=9600)

        #########################################
        ###               GET GPS             ###
        #########################################
        num = input("n° du port du GPS: ")
        if os.name == "nt":
            portname = "COM" + str(int(num))
        else:
            portname = "/dev/ttyUSB" + str(int(num))
        # portname = '/dev/ttyAMA0'
        self.gps = threading.Thread(name='Monitoring_GPS', target=getGPS, args=(portname, self.apc,))

        self.state = 'starting'

    def startMission(self):
        """
        Starting different threads linked to the mission
        """
        print("Starting Every Thread")

        # This thread is not started anymore. The communications is dealt with
        # the current FSM. We kept the object to use the serial port and its 
        # methods in order to encode messages.
        #self.apc.start()

        ## Tx to CC Thread
        self.apc_CC.start()
        ## GPS Thread
        self.gps.start()

        self.state = "searching"
 
    def shutdown(self):
        ## Rx from Beacons
        # self.apc.shutdown()
        ## Tx to CC Thread
        self.apc_CC.shutdown()
        ## GPS Thread
        self.gps.stop.set()
        ## Main thread
        self.stop.set() # TODO Check if it is useful
        self.state = None

    def searching(self):
        """
        Sending an hello message asking beacons to send their position if they 
        receive this message. The plane is flying.
        """

        #self.apc.serial.flushInput()
        #self.apc.serial.flushOutput()
        id_dest = 1                                     # Broadcast Adress
        message = ASP.GPS                               # To get beacons position
        self.apc.sendMessageGET('B', message, id_dest)
        self.state = 'waitingPing'

    def waitingPing(self):
        """
        Wait until we receive an answer from a beacon
        """
        TIME_PING = 0.5     # TODO Put that constant in at the beginning of the file

        t0 = time.time()
        reception = False

        #self.apc.serial.flushInput()
        #self.apc.serial.flushOutput()
        self.apc.rxbuff = self.apc.serial.read(ASP.GPS_SIZE)
        ## Check if the message we received has the correct length
        if len(self.apc.rxbuff) < ASP.GPS_SIZE:
            self.state = 'searching'
            sleep(TIME_PING)
            return

        ## Make a copy and reset the buffer
        frame = self.apc.rxbuff
        self.apc.rxbuff = bytearray()

        ## Unpack
        decoded_frame = struct.unpack(ASP.GPS_FRAME, frame)
        print("RX GPS : ", decoded_frame)

        # If the frame is valid according to our protocol
        if self.apc.validate(decoded_frame):
            
            id_beacon = decoded_frame[3]
            command = decoded_frame[4]
            lon = decoded_frame[5]
            lat = decoded_frame[6]
            alt = decoded_frame[7]
            print('id', id_beacon)

            ## Send acknowledgement
            self.apc.sendAck(id_beacon)
            reception = True # Breaking the loop

        else:
            print("test failed !")  


        if reception:
            saveData(decoded_frame)
            ## We received a message
            self.beacon_id = id_beacon
            self.state = 'askingData'

            ## Clean previous config of beacon
            ## For more explication, check the class constructor
            self.beacon_sensors_state = [0]*self.beacon_nb_sensors 
            self.beacon_sensor = 0     
            self.nb_of_reception = 0

            ## Repack a frame for tramission to the CC
            GPS = (lat, lon, alt)
            preamble_letter = 'A'

            ## Put in queue for transmission
            if not self.apc.aspbuff.full():
                priority = 2
                frame = self.apc.packMessageGPS(preamble_letter='A', GPS=GPS, id_dest=id_beacon)
                self.apc.aspbuff.put(Job(priority, frame))

        else:
            ## We did not receive a message
            self.state = 'searching'

    def askingData(self):
        time.sleep(0.15)
        
        ## Count how many time we asked the same question
        self.failsafe += 1

        ## Check if you retrieved data from each sensors
        if all([self.beacon_sensors_state[k] == 2 for k in range(self.beacon_nb_sensors)]):
            self.state = 'finishingCommunication'
            return
        if self.nb_of_reception == self.beacon_nb_sensors:
            self.state = 'finishingCommunication'
            return
        ## Check if we have in an infinite loop
        if self.failsafe == 15:
            print("\n\n----------------- FAILSAFE ---------------\n\n")
            self.failsafe = 0
            sleep(0.5)
            self.state = 'searching'

            ## Clean previous config of beacon
            self.beacon_sensors_state = [0]*self.beacon_nb_sensors # State of the different sensors 0 : unknown / 1 : asked / 2 : data retrieved
            self.beacon_sensor = 0     
            self.nb_of_reception = 0
            return 

        id_dest = self.beacon_id

        ## Flush the different buffers
        ## TODO : check if it's actually useful
        self.apc.serial.flushInput()
        self.apc.serial.flushOutput()

        ## Here comes the tricky part
        ## Selection of the correct index
        index = self.beacon_sensor
        if self.beacon_sensors_state[index] == 2:
            self.beacon_sensor += 1
            index += 1

        ## Selection of the message to send
        if index == 0:
            print("Temp")
            self.msg = ASP.TEMP
        elif index == 1:
            print("Humidity")
            self.msg = ASP.HUMIDITY
        ## End of the tricky part :p

        ## Send the message
        self.apc.sendMessageGET('B', msg, id_dest)

        ## Update the state of the sensor
        self.beacon_sensors_state[index] = 1

        ## Next state
        self.state = 'waitingData'

    def waitingData(self):
        TIME_PING = 0.15        # TODO : put that in another place
        time.sleep(TIME_PING)

        #self.apc.serial.flushInput()
        #self.apc.serial.flushOutput()

        reception = False
        self.apc.rxbuff = self.apc.serial.read(ASP.DATA_SIZE)

        ## Check the length of the received frame
        if len(self.apc.rxbuff) < ASP.DATA_SIZE:
            self.state = 'askingData'
            return

        ## Make a copy and reset the buffer
        frame = self.apc.rxbuff
        self.apc.rxbuff = bytearray()

        ## Unpack
        decoded_frame = struct.unpack(ASP.DATA_FRAME, frame)
        print("RX DATA : ", decoded_frame)

        ## If the frame is valid according to our protocol
        if self.apc.validate(decoded_frame):
            id_beacon = decoded_frame[3]
            command = decoded_frame[4]
            value = decoded_frame[5]

            ## Send acknowledgement
            self.apc.sendAck(id_beacon)
            reception = True # Breaking the loop

        else:
            print("test failed !")  

        if reception:
            ## We received a message
            saveData(decoded_frame)
            self.beacon_id = id_beacon
            self.state = 'askingData'
            self.nb_of_reception += 1

            ## Repack a frame for tramission to the CC
            preamble_letter = 'A'

            ## Put in queue for transmission
            if not self.apc.aspbuff.full():
                priority = 3

                # frame = self.apc.packMessageData(preamble_letter='A', command=command, data_tx=value, id_dest=0)
                frame = self.apc.packMessageGPS(preamble_letter='A', GPS=(value, 0, 0), id_dest=id_beacon, command=self.msg)
                self.apc.aspbuff.put(Job(priority, frame))

            ## Update the database of the beacon on the plane
            index = self.beacon_sensor
            self.beacon_sensors_state[index] = 2

        else:
            ## We did not receive a message
            self.state = 'askingData' # the state will analyse what to do after
                                      # depending on what we already received

    def finishingCommunication(self):
        """
        Tell the beacon to go to sleep
        """
        id_dest = self.beacon_id
        msg = ASP.SLEEP

        # Tell the beacon to go to sleep
        self.apc.sendMessageGET('B', msg, id_dest)
        time.sleep(0.5)
        self.apc.sendMessageGET('B', msg, id_dest)

        # Tell the control center that the communication is okay
        frame = self.apc.packMessageGPS(preamble_letter='A', GPS=(0, 0, 0), id_dest=id_dest, command=ASP.SLEEP)

        # Message and wait to start again
        self.apc.aspbuff.put(Job(3, frame))
        print("\n\n\n\n  \t\t\t FINISH !!!\n\n\n\n")

        ## Just for graphical purposes
        time.sleep(1)

        ## Go back the searching state
        self.state = 'searching'

    def receivingFile():
        self.state = 'finishingCommunication'


def saveData(frame):
    """
    Save data received in a file before transmission to the control center
    """
    command = frame[4]
    msg = frame[5]
    id_beacon = frame[3]

        
    if command == 102 :
        if id_beacon == 0: #Coordonnées GPS de l'avion
            categorie = 'GPS Avion'
        else :
            categorie = 'GPS Balise'
     
    elif command == 103 :
        # print("La température est de ", msg," degré")
        categorie = 'TEMP'

    elif command == ASP.HUMIDITY:
        categorie = "HUMIDITY"

    else:
        categorie = str(command)

    chemin1 = './donneesBalise_' + str(id_beacon) +'.txt' 
    chemin2 = './donneesGPSavion.txt'
    fichierDonnees = open(chemin1,'a')
    fichierAvion = open(chemin2,'a')
    
    if command == 102 and id_beacon == 0 :
        a = categorie + ' : longitude (x) : ' + str(frame[5]) + ' latitude (y) : '+ str(frame[6]) +'\n'
        fichierAvion.write(a)
    elif command == 102 and id_beacon != 0:
        a = categorie + ' : longitude (x) : ' + str(frame[5]) + ' latitude (y) : '+ str(frame[6]) +'\n'
        fichierDonnees.write(a)
    else :
        a = categorie + ' : ' + str(msg) +'\n'
        fichierDonnees.write(a)
    fichierDonnees.close()
    fichierAvion.close()

def main():
    raspberrypi = FSM()
    raspberrypi.start()

if __name__ == '__main__':
    """
    This is useful if you do not want to use the communication with the beacons
    for debugging purposes. It just sends GPS frame to the control center with
    the rpi position.
    """
    #########################################
    ###               TX to CC            ###
    #########################################
    num = input("n° du port APC control center: ")
    if os.name == "nt":
        portname = "COM" + str(int(num))
    else:
        portname = "/dev/ttyUSB" + str(int(num))
    apc_CC = ASP(portname, identity='controlcenterTX', name='Talking to CC', baudrate=19200)

    #########################################
    ###               GET GPS             ###
    #########################################
    num = input("n° du port du GPS: ")
    if os.name == "nt":
        portname = "COM" + str(int(num))
    else:
        portname = "/dev/ttyUSB" + str(int(num))

    ## If the GPS is connected to the GPIO pins.
    # portname = '/dev/ttyAMA0'
    gps = threading.Thread(name='Monitoring_GPS', target=getGPS, args=(portname, apc_CC, 4800,))

    apc_CC.start()
    gps.start()
    
