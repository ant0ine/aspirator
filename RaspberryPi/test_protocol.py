import struct
import serial
import time

serPort = "COM11"
baudRate = 9600
ser = serial.Serial(serPort, baudRate)

pre1=36     #preamble
pre2=65     #preamble
dir1=62     #direction of message
id1=10      #id of tag
size1=100   #size of message
command1=103#command
data1=0     #data
crc1=101    #checksum


# the <Lhhhb  is interpreted as follows
#   see https://docs.python.org/2/library/struct.html
#     <   little-endian
#     L   unsigned long
#     h   signed short
#     B   unsigned char - same as Arduino BYTE

# the following lines of code can be used to explore the packed data
data = struct.pack('<HHHHHHHH', pre1,pre2,dir1,id1,size1,command1,data1,crc1)
ser.write(data)
print(data)
time.sleep(1)
print(ser.readline())

ser.close()

#dataBack = unpack('<HHHHHHHH', dataToSend)
#print(dataBack)